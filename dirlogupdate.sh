#!/bin/bash

GLUPDATE='/jail/glftpd/bin/glupdate'
DIRCLEAN='/jail/glftpd/bin/olddirclean2'
MP3_NEW='/jail/glftpd/site/mp3'
MP3_ARCHIVE='/jail/glftpd/site/archive/mp3'
MP3_ARCHIVE_NO_PRE_TIME="${MP3_ARCHIVE}/_upload/_no_pretime"
CURRENT_YEAR=`date +%Y`

ACTION=${1:-NEW}
ARCHIVE_YEAR=${2:-$CURRENT_YEAR}

function glupdateNew() {
	ls -1 $MP3_NEW | while read d
	do
		upatePath $MP3_NEW/$d
	done
}

function glupdateArchiveAll() {
	ls -1 $MP3_ARCHIVE | egrep -v '^_.*' | while read y
	do
		glupdateArchiveYear $y
	done
	upatePath $MP3_ARCHIVE_NO_PRE_TIME
}

function glupdateArchiveYear() {
	if [ -n "$1" ] &&  [ -d "$MP3_ARCHIVE/$1" ]
	then
		ls -1 $MP3_ARCHIVE/$1 | while read d
		do
			upatePath $MP3_ARCHIVE/${1}/${d}
		done
	fi
}

function upatePath() {
	if [ -n "$1" ]
	then
		echo "Upating $1"
		$GLUPDATE $1 > /dev/null 2>&1
	fi
}

function cleanUp() {
	echo "Cleanup old entries"
	$DIRCLEAN -PD > /dev/null 2>&1
}


case $ACTION in
	NEW)
		glupdateNew
		;;
	ARCHIVE)
		glupdateArchiveYear $ARCHIVE_YEAR
		;;
	CLEAN)
		cleanUp
		;;
	ALL)
		glupdateArchiveAll
		glupdateNew
		cleanUp
		;;
	*)
		echo "Unknown action '$ACTION' selected"
		exit 1
		;;
esac
