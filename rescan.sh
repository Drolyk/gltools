#!/bin/bash

GLFTPD_ROOT='/jail/glftpd'
RESCAN="${GLFTPD_ROOT}"/bin/rescan'
MP3_NEW='/site/mp3'
FLAC_NEW='/site/flac'
MP3_ARCHIVE='/site/archive/mp3'

ACTION=${1:-NEW}
ARCH_PART=${2:-2013}
SKIP_REGEXP='/(_weird_shit|_move|_bad_genre|_delpred|_to_move)/'

function dirName() {
	if [ -d $1 ]; then
		echo $(cd "$1" && pwd)
	fi
}

function rescanRecursively() {
	DIR=$(dirName ${GLFTPD_ROOT}/${1})
	SKIP=${2:-\\s{100\}}
	if [ -n "$DIR" ] && [ -d $DIR ] ; then
		find $DIR -iname "*.sfv" -type f | egrep -v "$SKIP" | while read f; do
			d=$(dirname "$f" | sed "s|^${GLFTPD_ROOT}||")
			$RESCAN --chroot="$GLFTPD_ROOT" --dir="${d}"
		done
	fi
}


case $ACTION in
	NEW)
		rescanRecursively $MP3_NEW
		rescanRecursively $FLAC_NEW
		;;
	ARCHIVE)
		rescanRecursively "${MP3_ARCHIVE}/${ARCH_PART}" $SKIP_REGEXP
		;;
	*)
		echo "Unknown action '$ACTION' selected"
		exit 1
		;;
esac
