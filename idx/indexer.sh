#!/bin/bash
# 
# Archive and Incoming indexer
# drolyk@gmail.com, 2013
#

SETTINGS_FILE="indexer-conf.sh"
SETTINGS_DIR=`dirname "$0"`
SETTINGS_PATH=$SETTINGS_DIR/$SETTINGS_FILE

if [ ! -r $SETTINGS_PATH ]; then
	echo "Can't load setting from $SETTINGS_PATH"
	exit 1
fi

source $SETTINGS_PATH

INDEX_ROOT="${FTPROOT}/${INDEXT_PATH}"
YEAR_REGEXP='^(19[0-9][0-9x]|20[0-2][0-9x])$'
SKIP='(incomplete)|(no-nfo)'
GRP_SUFFIXES='_house\|_classics\|_classic\|_web\|_256\|_live\|-vbr'

function map_put {
	# 1:map 2:key 3:value
	eval "$1""$2"='$3'
}

function map_get {
	# 1:map 2:key
	eval echo '${'"$1$2"'#hash}'
}

function map_name {
	echo `basename $1` | sed 's/[^a-zA-Z]\+/_/g'
}

function release_tag {
	RELEASE=$1
	TAG=$2
	DEFAULT=${3:-Unknown}
	MAP=`map_name $RELEASE`
	VALUES=""
	ls -1 $RELEASE | egrep -i "*.mp3$" | {
		while read f; do
			t=`${MPINFO} -p "$TAG" "$RELEASE/$f" 2>/dev/null | sed 's/[^a-zA-Z0-9]/_/g'`
			T_COUNT=`map_get "$MAP" "$t"`
			if [ "x$T_COUNT" == "x" ]; then
				T_COUNT=0
			fi
			T_COUNT=`expr $T_COUNT + 1`
			map_put "$MAP" "$t" "$T_COUNT"
			export VALUES="${VALUES};${t}"
		done
		
		VALUE=$DEFAULT;
		MAX_COUNT=0
		while IFS=';' read -ra ADDR; do
			for t in "${ADDR[@]}"; do
				COUNT=`map_get "$MAP" "$t"`
				if [ "x$COUNT" != "x" ] && [ "x$t" != "x" ] && [ $COUNT -gt $MAX_COUNT ]; then
					MAX_COUNT=$COUNT
					VALUE="$t"
				fi
			done
		done <<< "$VALUES"
		echo $VALUE
	}
}

function is_release_va {
	RNAME=$1
	if echo $RNAME | egrep -qi '^(va[_-]|various_artists).*'; then
		return 0
	else
		return 1
	fi
}

function is_release_sat {
	RNAME=$1
	if echo $RNAME | egrep -qi '[0-9]{2}[._-][0-9]{2}[._-][0-9]{4}|[._-](SBD|SAT|CABLE|FM|DVBC|DVB|DVBS|LINE)[._-][0-9]{2,4}'; then
		return 0
	else
		return 1
	fi
}

function is_release_fix {
	RNAME=$1
	if echo $RNAME | egrep -qi 'nfo[_-]?fix|dir[_-]?fix'; then
		return 0
	else
		return 1
	fi
}

function is_release_year {
	YEAR=$1
	if echo $YEAR | egrep -qi "$YEAR_REGEXP"; then
		return 0
	else
		return 1
	fi
}

function release_year_from_name {
	RNAME=`basename $1`
	for FIELD in `echo $RNAME | sed 's/-/\n/g' | egrep -i "$YEAR_REGEXP"`; do
		YEAR="$FIELD"
	done
	echo $YEAR
}

function release_year {
	RNAME=`basename $1`
	NO_YEAR="_no_year"

	if `is_release_fix "$RNAME"`; then
		YEAR=`release_year_from_name "$RNAME"`
	else
		YEAR=`release_tag "$1" "%y" "$NO_YEAR"`
		if ! `is_release_year "$YEAR"`; then
			YEAR=`release_year_from_name "$RNAME"`
		fi
	fi

	if ! `is_release_year "$YEAR"`; then
		YEAR=$NO_YEAR
	fi

	echo $YEAR | tr '[:upper:]' '[:lower:]'
}

function release_genre {
	RNAME=`basename $1`
	NO_GENRE="_no_genre"

	if `is_release_fix "$RNAME"`; then
		GENRE="_fixes"
	else
		GENRE=`release_tag "$1" "%g" "$NO_GENRE"`
	fi

	if [ "x$GENRE" == "x" ]; then
		GENRE=$NO_GENRE
	fi

	echo $GENRE | tr '[:upper:]' '[:lower:]'
}

function release_group {
	RNAME=`echo $1 | sed 's/_\?[Ii][Nn][Tt])\?$//i'`
	NO_GRP="_no_grp"

	GRP_SUFFIX=`echo $RNAME | sed "s/.*\($GRP_SUFFIXES\)\$/\1/i"`
	if [ "$GRP_SUFFIX" != "$RNAME" ]; then
		RNAME=`echo $RNAME | sed s/${GRP_SUFFIX}\$//`
	fi

	for FIELD in `echo $RNAME | sed 's/[()_-]/ /g'`; do
		GRP="$FIELD"
	done

	if `is_release_year $GRP`; then
		if [ "x$RNAME" != "x$1" ]; then
			if [ "x$GRP_SUFFIX" != "x$RNAME" ]; then
				GRP=`echo $GRP_SUFFIX | sed 's/^[_-]//'`
			else
				GRP="int"
			fi
		else
			GRP=$NO_GRP
		fi
	fi

	if [ "x$GRP" == "x" ]; then
		GRP=$NO_GRP
	fi

	echo $GRP | tr '[:upper:]' '[:lower:]'
}

function release_date {
	YEAR="$1"
	DAY_DIR="$2"
	
	if [ "x$YEAR" = "x$SOURCE_INCOMING_IDX" ]; then
		YEAR=`date +%Y`
	else 
		YEAR=`echo $YEAR | sed -r 's/[^0-9]/0/g'`
		if [ $YEAR -le 1900 ]; then
			YEAR="1970"
		fi
	fi

	MMDD=`echo $DAY_DIR | sed -r "$SOURCE_DAYDIR_SED"`
	if [ ${#MMDD} -ne 4 ]; then
		MMDD='0101'
	fi
	echo "::RELEASE DATE [${YEAR}|${MMDD}]" >> $LOG_PATH
	echo "${YEAR}${MMDD}"
}

function fix_path {
	echo $1 | sed 's/\/\+/\//g'
}

function mkrelative {
	SRC=$1
	DST=$2
	COMMON=$SRC
	BACK=
	while [ "${DST#$COMMON}" = "$DST" ]; do
		COMMON=`dirname $COMMON`
		BACK="../$BACK"
	done
	echo `fix_path ${BACK}${DST#$COMMON}`
}

function mk_index_link {
	IDX_PATH=$1
	R_PATH=$2
	TIME="${3}0000.00"
	R_NAME=`basename $2`
	REL_PATH=`mkrelative $IDX_PATH $R_PATH`
	echo "::R_NAME [${R_NAME}]" >> $LOG_PATH
	echo "::IDX_PATH [${IDX_PATH}]" >> $LOG_PATH 
	echo "::REL_PATH [${REL_PATH}]" >> $LOG_PATH
	(
		mkdir -p $IDX_PATH
		echo "::MkDir [$IDX_PATH]" >> $LOG_PATH
		cd $IDX_PATH
		if [ ! -L $R_NAME ]; then
			ln -s $REL_PATH .
		fi
		touch -h -c -t "$TIME" "$R_NAME"
	)
}

function process_release {
	SECTION_BASE=$1
	DAY_DIR=$2
	RELEASE=$3

	echo -n "$RELEASE"

	GENRE=`release_genre "$DAY_DIR/$RELEASE"`
	YEAR=`release_year "$DAY_DIR/$RELEASE"`
	GRP=`release_group "$RELEASE"`
	DATE=`release_date "$SECTION_BASE" "$DAY_DIR"`

	YEAR_PART="$YEAR"
	GENRE_PART="$GENRE"

	if `is_release_va "$RELEASE"`; then
		YEAR_PART="$YEAR/$INDEX_VA"
		GENRE_PART="$GENRE/$INDEX_VA"
	fi

	if `is_release_sat "$RELEASE"`; then
		YEAR_PART="$YEAR/$INDEX_SAT"
		GENRE_PART="$GENRE/$INDEX_SAT"
	fi

	echo " [$GENRE_PART] [$YEAR_PART] [$GRP]"

	echo "::Release Process [$SECTION_BASE] [$DAY_DIR] [$RELEASE]"
	echo "::Release Process [$SECTION_BASE] [$DAY_DIR] [$RELEASE]" >> $LOG_PATH

	# create tree sorted by pre.date
	mk_index_link "$INDEX_ROOT/$INDEX_SECTION_DAYDIR/$SECTION_BASE/$INDEX_PART_GENRE/$GENRE_PART" "$DAY_DIR/$RELEASE" "$DATE"
	mk_index_link "$INDEX_ROOT/$INDEX_SECTION_DAYDIR/$SECTION_BASE/$INDEX_PART_GROUP/$GRP" "$DAY_DIR/$RELEASE" "$DATE"
	mk_index_link "$INDEX_ROOT/$INDEX_SECTION_DAYDIR/$SECTION_BASE/$INDEX_PART_YEAR/$YEAR_PART" "$DAY_DIR/$RELEASE" "$DATE"
	# create tree sorted by release.date
	mk_index_link "$INDEX_ROOT/$INDEX_SECTION_RELEASE_DATE/$YEAR/$INDEX_PART_GENRE/$GENRE_PART" "$DAY_DIR/$RELEASE" "$DATE"
	mk_index_link "$INDEX_ROOT/$INDEX_SECTION_RELEASE_DATE/$YEAR/$INDEX_PART_GROUP/$GRP" "$DAY_DIR/$RELEASE" "$DATE"
	# create tree sorted by grp
	mk_index_link "$INDEX_ROOT/$INDEX_SECTION_BY_GRP/$GRP" "$DAY_DIR/$RELEASE" "$DATE"
}

function process_section {
	SECTION="$1"
	SECTION_BASE=`basename $2`

	echo " --- processing $SECTION"
	mkdir -p "$INDEX_ROOT/$INDEX_SECTION_DAYDIR/$SECTION_BASE"
	mkdir -p "$INDEX_ROOT/$INDEX_SECTION_RELEASE_DATE"
	touch "$INDEX_ROOT/$INDEX_SECTION_DAYDIR/$SECTION_BASE/$WRN_MSG"
	touch "$INDEX_ROOT/$INDEX_SECTION_RELEASE_DATE/$WRN_MSG"
	ls -1 "$SECTION" | egrep -i "$SOURCE_DAYDIR" | while read d; do
		DAY_DIR="$SECTION/$d"
		echo " ---> [$SECTION_BASE] [$d]"
		ls -1 "$DAY_DIR" | egrep -vi "$SKIP" | while read r; do
			PROPER=`echo ${r} | grep -- "-"`
			if [ "${PROPER}" != "" ]; then
				process_release "$SECTION_BASE" "$DAY_DIR" "$r"
			fi
		done
	done
}

function update_message {
	UPDATE_PATH=$1
	DATE=`date +%d.%m.%Y`
	mkdir -p $UPDATE_PATH
	rm -rf $UPDATE_PATH/${UPD_MSG}.*
	touch "$UPDATE_PATH/${UPD_MSG}.${DATE}"
}

function cleanup_index {
	CLEANUP_PATH=$1
	echo " *** Cleanup $CLEANUP_PATH"
	mkdir -p "$CLEANUP_PATH"
	find $CLEANUP_PATH -type l ! -readable -exec rm -f {} \;
}

function cleanup_by_pre {
	CLEANUP_PATH="$INDEX_ROOT/$INDEX_SECTION_DAYDIR/$1"
	if [ "x$1" == "x" ]; then
		ls -1 $CLEANUP_PATH | while read p; do
			update_message $CLEANUP_PATH/$p
		done
	else
		update_message $CLEANUP_PATH
	fi
	cleanup_index "$CLEANUP_PATH"
}

function cleanup_by_date {
	CLEANUP_PATH="$INDEX_ROOT/$INDEX_SECTION_RELEASE_DATE"
	update_message $CLEANUP_PATH
	cleanup_index $CLEANUP_PATH
}

function cleanup_by_grp {
	CLEANUP_PATH="$INDEX_ROOT/$INDEX_SECTION_BY_GRP"
	update_message $CLEANUP_PATH
	cleanup_index $CLEANUP_PATH
}


function process_incoming {
	process_section "$FTPROOT/$SOURCE_INCOMING" "$SOURCE_INCOMING_IDX"
}

function process_archive {
	if [ "x${1}" == "x" ]; then
		ls -1 "$FTPROOT/$SOURCE_ARCHIVE" | egrep -i '^[0-9x]{4}$' | sort | while read p; do
			process_section "$FTPROOT/$SOURCE_ARCHIVE/$p" "$p"
		done
	else
		process_section "$FTPROOT/$SOURCE_ARCHIVE/$1" "$1"
	fi
}

function on_exit {
	INDEX_DAYDIR=$INDEX_ROOT/$INDEX_SECTION_DAYDIR
	ls -1 $INDEX_DAYDIR | while read p; do
		rm -rf $INDEX_DAYDIR/$p/$WRN_MSG
	done
	rm -rf $INDEX_ROOT/$INDEX_SECTION_RELEASE_DATE/$WRN_MSG
}

trap on_exit EXIT

# commandline params
SECTION=${1}
SECTION_PART=${2}

case $SECTION in
	all)
		process_incoming
		process_archive
		cleanup_by_pre
		cleanup_by_date
		;;
	inc)
		process_incoming
		cleanup_by_pre $SOURCE_INCOMING_IDX
		cleanup_by_date
		;;
	arch)
		process_archive $SECTION_PART
		cleanup_by_pre $SECTION_PART
		cleanup_by_date
		cleanup_by_grp
		;;
	inc-idx)
		SECTION_PATH=$2
		RELEASE=$3
		DAYDIR=`fix_path $FTPROOT/$SECTION_PATH`
		process_release "$SOURCE_INCOMING_IDX" "$DAYDIR" "$RELEASE"
		update_message $INDEX_ROOT/$INDEX_SECTION_DAYDIR/$SOURCE_INCOMING_IDX
		update_message $INDEX_ROOT/$INDEX_SECTION_RELEASE_DATE
		;;
	arch-idx)
		SECTION_BASE=$2
		SECTION_PATH=$3
		RELEASE=$4
		DAYDIR=`fix_path $FTPROOT/$SECTION_PATH`
		process_release "$SECTION_BASE" "$DAYDIR" "$RELEASE"
		update_message $INDEX_ROOT/$INDEX_SECTION_DAYDIR/$SECTION_BASE
		update_message $INDEX_ROOT/$INDEX_SECTION_RELEASE_DATE
		;;
	*)
		echo "Usage: $0 [all|inc|arch|inc-idx|arch-idx] "
		echo "Examples:"
		echo "  ${0} all - to index incoming and all archive sections"
		echo "  ${0} inc - to index incoming only"
		echo "  ${0} arch - to index archive only"
		echo "  ${0} arch 2012 - to index 2012 section of archive"
		echo "  ${0} inc-idx '/site/incoming/0602' 'Flatlex-Breeze-ETTR010-WEB-2013-TBM' - index single release from incoming"
		echo "  ${0} arch-idx '2012' '/site/archive/2013/0107' 'Raggapop_Inc_-_Bulletproof_(13BMP3055)-WEB-2012-TR' - index single release from 2012 archive section with given path"
		;;
esac
